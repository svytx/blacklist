#!/bin/bash

DIR="$(CDPATH= cd -- "$(dirname -- "$0")" && pwd -P)"

gen() {
    cd "$DIR/vendor/dnscrypt-proxy/utils/generate-domains-blacklists"
    python2 ./generate-domains-blacklist.py -w "$DIR/whitelist.txt" "$@"
}

gen > "$DIR/tmp/dnscrypt-blacklist-domains-default.txt"

echo "file:$DIR/tmp/dnscrypt-blacklist-domains-default.txt" > "$DIR/tmp/blacklist.conf"
cat "$DIR/blacklist.conf" >> "$DIR/tmp/blacklist.conf"
gen -c "$DIR/tmp/blacklist.conf" > "$DIR/tmp/dnscrypt-blacklist-domains.txt"

sudo cp -v "$DIR/tmp/dnscrypt-blacklist-domains.txt" "/etc/dnscrypt-proxy/blacklist.txt"
sudo systemctl restart dnscrypt-proxy.service
